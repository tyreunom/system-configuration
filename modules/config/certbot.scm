;;; Tyreunom's system administration and configuration tools.
;;;
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;
;; Certbot configuration tools
;;

(define-module (config certbot)
  #:use-module (gnu services)
  #:use-module (gnu services certbot)
  #:use-module (guix gexp)
  #:export (certbot-service))

(define %nginx-deploy-hook
  (program-file
   "nginx-deploy-hook"
   #~(let ((pid (call-with-input-file "/var/run/nginx/pid" read))
           (cert-dir (getenv "RENEWED_LINEAGE")))
       (let ((privkey (string-append cert-dir "/privkey.pem")))
         ;; certbot private keys are world-readable by default, and smptd complains
         ;; about that, refusing to start :/
         (chmod privkey #o6000))
       (kill pid SIGHUP))))

(define AT "@")

(define (certbot-service domains-list)
  (service certbot-service-type
    (certbot-configuration
      (email (string-append "certs" AT "lepiller.eu"))
      (webroot "/srv/http/certbot")
      (rsa-key-size 4096)
      (certificates
        (map
          (lambda (domains)
            (certificate-configuration
              (domains domains)
              (deploy-hook %nginx-deploy-hook)))
          domains-list)))))
