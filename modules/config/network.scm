;;; Tyreunom's system administration and configuration tools.
;;;
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;
;; Network configuration tools
;;

(define-module (config network)
  #:use-module (data dns)
  #:use-module (gnu packages linux)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (guix gexp)
  #:export (hermes-network-service))

(define (iproute2-shepherd-service config)
  (list (shepherd-service
	  (documentation "Run the iproute2 network service")
	  (provision '(networking))
	  (requirement '())
	  (start #~(lambda _
		     (let ((ip (string-append #$iproute "/sbin/ip"))
			   (dev "ens18"))
		       (invoke ip "a" "add" (string-append #$hermes-ip4 "/27") "dev" dev)
		       (invoke ip "l" "set" dev "up")
		       (invoke ip "-6" "a" "add" (string-append #$hermes-ip6 "/48")
			       "dev" dev)
		       (invoke ip "r" "add" "default" "via" "89.234.186.97" "dev" dev)
		       (invoke ip "-6" "r" "add" "default" "via"
			       "fe80::204:92:100:1" "dev" dev))))
	  (stop #~(lambda _
		    (display "Cannot stop iproute2 service.\n"))))))

(define iproute2-service-type
  (service-type (name 'static-networking)
		(extensions
		  (list
		    (service-extension shepherd-root-service-type
				       iproute2-shepherd-service)))
		(description "")))

(define hermes-network-service
  (service iproute2-service-type #t))
