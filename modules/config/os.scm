;;; Tyreunom's system administration and configuration tools.
;;;
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;
;; OS template parts for different usages
;;

(define-module (config os)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages android)
  #:use-module (gnu packages certs)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages openbox)
  #:use-module (gnu packages ssh)
  #:use-module (gnu packages tmux)
  #:use-module (gnu packages vim)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xfce)
  #:use-module (gnu services)
  #:use-module (gnu services admin)
  #:use-module (gnu services base)
  #:use-module (gnu services dbus)
  #:use-module (gnu services desktop)
  #:use-module (gnu services networking)
  #:use-module (gnu services ssh)
  #:use-module (gnu services virtualization)
  #:use-module (gnu services xorg)
  #:use-module (gnu system)
  #:use-module (gnu system accounts)
  #:use-module (gnu system file-systems)
  #:use-module (gnu system keyboard)
  #:use-module (gnu system locale)
  #:use-module (gnu system shadow)
  #:use-module (guix gexp)
  #:use-module (config rotation)
  #:export (server-services
            desktop-services
            tyreunom-os
            tyreunom-desktop-os))

(define (server-services host-name)
  (cons*
    (service ntp-service-type)
    (service openssh-service-type
             (openssh-configuration
               (authorized-keys
                 `(("tyreunom" ,(local-file "../../keys/tyreunom.pub"))))))
    (simple-service 'motd-service etc-service-type
      `(("motd" ,(local-file (string-append "motd/" host-name)))))
    (modify-services %base-services
      (rottlog-service-type config =>
        server-rotation-service-config)
      (login-service-type config =>
        (login-configuration
          (inherit config)
          (motd (local-file (string-append "motd/" host-name)))))
      (guix-service-type config =>
        (guix-configuration
          (inherit config)
          (substitute-urls '("https://ci.guix.gnu.org")))))))

(define desktop-services
  (cons*
    (simple-service 'dconf dbus-root-service-type (list dconf))
    (service tor-service-type)
    (service qemu-binfmt-service-type
      (qemu-binfmt-configuration
        (platforms (lookup-qemu-platforms "arm" "aarch64" "i686" "ppc"))))
    (modify-services %desktop-services
      (rottlog-service-type config =>
        desktop-rotation-service-config)
      (udev-service-type config =>
        (udev-configuration
          (inherit config)
          (rules (cons* android-udev-rules
                        (udev-configuration-rules config)))))
      (gdm-service-type config =>
       (gdm-configuration
         (inherit config)
         (xorg-configuration
           (xorg-configuration
             (keyboard-layout (keyboard-layout "fr" "bepo"))))))
      (guix-service-type config =>
        (guix-configuration
          (inherit config)
          (substitute-urls '("https://ci.guix.gnu.org")))))))

(define (tyreunom-os host-name)
  (operating-system
    (host-name host-name)
    (timezone "Europe/Paris")
    (locale "fr_FR.UTF-8")
    (keyboard-layout (keyboard-layout "fr" "bepo"))
    (bootloader #f)
    (file-systems %base-file-systems)
    (users (cons (user-account
                   (name "tyreunom")
                   (group "users")
                   (home-directory "/home/tyreunom"))
                 %base-user-accounts))
    (locale-definitions
      (cons (locale-definition
              (name "eo.utf8") (source "eo"))
            %default-locale-definitions))
    (hosts-file
      (plain-file "hosts"
        (string-append "127.0.0.1 lepiller.eu localhost " host-name "\n"
                       "::1       lepiller.eu localhost " host-name "\n"
                       %facebook-host-aliases)))
    (packages (cons* openssh tmux neovim nss-certs %base-packages))
    (services %base-services)))

(define (tyreunom-desktop-os host-name)
  (let ((system (tyreunom-os host-name)))
    (operating-system
      (inherit system)
      (users
        (map (lambda (user)
               (if (equal? (user-account-name user) "tyreunom")
                   (user-account
                     (inherit user)
                     (supplementary-groups '("netdev" "adbusers" "audio" "video" "kvm")))
                   user))
             (operating-system-users system)))
      (groups (cons (user-group (system? #t) (name "adbusers"))
                    %base-groups))
      (hosts-file
        (plain-file "hosts"
          (string-append "127.0.0.1 " host-name ".lepiller.eu localhost " host-name "\n"
                         "::1       " host-name ".lepiller.eu localhost " host-name "\n"
                         %facebook-host-aliases)))
      (packages (cons* swaylock wpa-supplicant gvfs openbox xfce4-terminal
                       (operating-system-packages system))))))
