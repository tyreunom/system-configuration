;;; Tyreunom's system administration and configuration tools.
;;;
;;; Copyright © 2020 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;
;; Definition of rotation policies
;;

(define-module (config iptables)
  #:use-module (gnu services)
  #:use-module (gnu services networking)
  #:use-module (guix gexp)
  #:export (lepiller-iptables-service))

(define ipv4-config
  (plain-file "iptables.rules" "*filter
:INPUT ACCEPT
:FORWARD ACCEPT
:OUTPUT ACCEPT
-A INPUT -p tcp --dport 22 -m state --state NEW -m recent --update --seconds=60 --hitcount 4 -j REJECT --reject-with icmp-port-unreachable
-A INPUT -p tcp --dport 22 -m state --state NEW -m recent --set
COMMIT
"))

(define ipv6-config
  (plain-file "ip6tables.rules" "*filter
:INPUT ACCEPT
:FORWARD ACCEPT
:OUTPUT ACCEPT
-A INPUT -p tcp --dport 22 -m state --state NEW -m recent --update --seconds=60 --hitcount 4 -j REJECT --reject-with icmp6-port-unreachable
-A INPUT -p tcp --dport 22 -m state --state NEW -m recent --set
COMMIT
"))

(define lepiller-iptables-service
  (service iptables-service-type
    (iptables-configuration
      (ipv4-rules ipv4-config)
      (ipv6-rules ipv6-config))))
