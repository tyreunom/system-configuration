;;; Tyreunom's system administration and configuration tools.
;;;
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (config web)
  #:export (web-base-policy
            web-html-policy
	    default-web-policy
        accept-languages))

(define (accept-languages language-lists)
  "Returns nginx configuration code to set up the $lang variable
according to the Accept-Language header in the HTTP request.  The
requesting user agent will be served the files at /$lang/some/url.
Each list in LANGUAGE-LISTS starts with the $lang and is followed by
synonymous IETF language tags that should be mapped to the same $lang."
  (define (language-mappings language-list)
    (define (language-mapping language)
      (string-join (list "    "  language (car language-list) ";")))
    (string-join (map language-mapping language-list) "\n"))

  (let ((directives
         `(,(string-join
             `("set_from_accept_language $lang_unmapped"
               ,@(map string-join language-lists)
               ";"))
           "map $lang_unmapped $lang {"
           ,@(map language-mappings language-lists)
           "}")))
    directives))

(define web-base-policy
  '("add_header Strict-Transport-Security \"max-age=31536000; includeSubDomains\" always;"
    "add_header X-Frame-Options DENY;"
    "add_header X-Content-Type-Options nosniff;"
    "add_header Content-Security-Policy 'default-src \\'none\\'; img-src \\'self\\'; style-src \\'self\\' \\'unsafe-inline\\'; frame-ancestors \\'none\\'';"
    "add_header Referrer-Policy no-referrer;"))

(define (web-html-policy additional-data)
  (append
    '("location ~ \\.html$ {"
      "    add_header 'Cache-Control' 'no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0';"
      "    add_header Strict-Transport-Security \"max-age=31536000; includeSubDomains\" always;"
      "    add_header X-Frame-Options DENY;"
      "    add_header X-Content-Type-Options nosniff;"
      "    add_header Content-Security-Policy 'default-src \\'none\\'; img-src \\'self\\'; style-src \\'self\\' \\'unsafe-inline\\'; frame-ancestors \\'none\\'';"
      "    add_header Referrer-Policy no-referrer;")
    additional-data
    '("    expires off;"
      "}")))

(define default-web-policy
  (append
    web-base-policy
    (web-html-policy '())))
