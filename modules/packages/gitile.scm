;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Some of the help text was taken from the default dovecot.conf files.

(define-module (packages gitile)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages guile-xyz)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages tls)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix licenses))

(define my-guile-git
  (package
    (inherit guile-git)
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://gitlab.com/roptat/guile-git")
                     (commit "c39ab944d8004d3ab751a9e27336469afec081eb")))
              (file-name (git-file-name "guile-git" "0.4.0.c39ab94"))
              (sha256
               (base32
                "0hknsv9r7pjahmxkvd7zpz93saki5kymh88xs6pb4h9d0ssp4fmp"))))
    (native-inputs
     `(("autoconf" ,autoconf)
       ("automake" ,automake)
       ("texinfo" ,texinfo)
       ,@(package-native-inputs guile-git)))))

(define my-guile-syntax-highlight
  (package
    (inherit guile-syntax-highlight)
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://git.dthompson.us/guile-syntax-highlight.git")
                     (commit "51727cbb7fc05ef743aab2d7b16314ea1ed790e4")))
              (file-name (git-file-name "guile-syntax-highlight" "0.1.51727cb"))
              (sha256
               (base32
                "1cvacy4y5qxajygb1qg8hvhjdf2xnnwnm5j918cabsi8wfwchig7"))))
    (native-inputs
     `(("autoconf" ,autoconf)
       ("automake" ,automake)
       ("texinfo" ,texinfo)
       ,@(package-native-inputs guile-syntax-highlight)))))

(define-public gitile
  (let ((commit "fac1f0ccdbaca794451ae43526b1bbffacd8161a")
	(revision "0"))
    (package
      (name "gitile")
      (version (git-version "0.0" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                       (url "https://git.lepiller.eu/git/gitile")
                       (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "048pm743gnxashggngw3zlsr13wf0k686n3j23czc4988gz7mfvp"))))
      (build-system gnu-build-system)
      (arguments
       `(#:modules ((guix build utils)
                    (guix build gnu-build-system)
                    (ice-9 rdelim)
                    (ice-9 popen))
         #:make-flags (list "GUILE_AUTO_COMPILE=0")
         #:phases
         (modify-phases %standard-phases
           (add-after 'install 'install-bin
             (lambda* (#:key outputs #:allow-other-keys)
               (install-file "scripts/gitile"
                             (string-append (assoc-ref outputs "out")
                                            "/bin"))
               #t))
           (add-after 'install-bin 'wrap-program
               (lambda* (#:key inputs outputs #:allow-other-keys)
                 ;; Wrap the 'cuirass' command to refer to the right modules.
                 (let* ((out    (assoc-ref outputs "out"))
                        (commonmark (assoc-ref inputs "guile-commonmark"))
                        (git    (assoc-ref inputs "guile-git"))
                        (bytes  (assoc-ref inputs "guile-bytestructures"))
                        (fibers (assoc-ref inputs "guile-fibers"))
                        (gcrypt (assoc-ref inputs "guile-gcrypt"))
			(syntax-highlight (assoc-ref inputs "guile-syntax-highlight"))
                        (deps   (list out commonmark git bytes fibers gcrypt
				      syntax-highlight))
                        (guile  (assoc-ref %build-inputs "guile"))
                        (effective (read-line
                                    (open-pipe* OPEN_READ
                                                (string-append guile "/bin/guile")
                                                "-c" "(display (effective-version))")))
                        (mods   (string-drop-right  ;drop trailing colon
                                 (string-join deps
                                              (string-append "/share/guile/site/"
                                                             effective ":")
                                              'suffix)
                                 1))
                        (objs   (string-drop-right
                                 (string-join deps
                                              (string-append "/lib/guile/" effective
                                                             "/site-ccache:")
                                              'suffix)
                                 1)))
                   (wrap-program (string-append out "/bin/gitile")
                     `("GUILE_LOAD_PATH" ":" prefix (,mods))
                     `("GUILE_LOAD_COMPILED_PATH" ":" prefix (,objs)))
                   #t))))))
      (native-inputs
       `(("autoconf" ,autoconf)
         ("automake" ,automake)
         ("guile" ,guile-3.0)
         ("pkg-config" ,pkg-config)))
      (inputs
       `(("guile" ,guile-3.0)
	 ("guile-commonmark" ,guile-commonmark)
         ("guile-fibers" ,guile-fibers)
         ("guile-gcrypt" ,guile-gcrypt)
         ("guile-git" ,my-guile-git)
	 ("guile-syntax-highlight" ,my-guile-syntax-highlight)
         ("gnutls" ,gnutls)))
      (home-page "")
      (synopsis "")
      (description "")
      (license gpl3+))))

gitile
