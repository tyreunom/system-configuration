;;; Tyreunom's system administration and configuration tools.
;;;
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;
;; DNS-related data
;;

(define-module (data dns)
  #:export (ene-rennes-ip4 ene-kb-ip4 ene-kb-ip6
            hermes-ip4 hermes-ip6
            xana-ip4))

(define hermes-ip4 "89.234.186.109")
(define hermes-ip6 "2a00:5884:8208::1")

(define ene-rennes-ip4 "79.91.200.80")
(define ene-kb-ip4 "86.247.139.155")
(define ene-kb-ip6 "2a01:cb04:717:4400:cf:5ff:fe81:68af")

(define xana-ip4 "62.210.81.154")
