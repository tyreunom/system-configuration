;;; Tyreunom's system administration and configuration tools.
;;;
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (gnu))
(use-modules (gnu system))
(use-modules (gnu bootloader) (gnu bootloader grub))
(use-modules (gnu services dns))
(use-modules (gnu services linux))
(use-modules (gnu services sound))
(use-modules (gnu services virtualization))
(use-modules (config os))

(let ((system (tyreunom-desktop-os "tachikoma")))
  (operating-system
    (inherit system)
    (bootloader
      (bootloader-configuration
        (target "/dev/sda")
        (bootloader grub-bootloader)))
    (mapped-devices
      (list (mapped-device
              (source (uuid "c61efb54-dd82-4f94-ba30-344ed71d0783"))
              (target "cryptroot")
              (type luks-device-mapping))))
    (file-systems (cons (file-system
                          (device "/dev/mapper/cryptroot")
                          (mount-point "/")
                          (type "ext4")
                          (dependencies mapped-devices))
                        %base-file-systems))
    (users (map
             (lambda (user)
               (if (equal? (user-account-name user) "tyreunom")
                 (user-account
                   (inherit user)
                   (supplementary-groups
                     (cons "libvirt" (user-account-supplementary-groups user))))
                 user))
             (operating-system-users system)))
    (services
      (cons*
        (service zram-device-service-type
                 (zram-device-configuration
                   (size "2G")
                   (compression-algorithm 'zstd)))
        (service libvirt-service-type
                 (libvirt-configuration
                   (unix-sock-group "libvirt")))
        (service virtlog-service-type
                 (virtlog-configuration
                   (max-clients 1000)))
	(service dnsmasq-service-type
	  (dnsmasq-configuration
	    (no-resolv? #t)
	    (servers '("80.67.169.12"
		       "80.67.169.40"
		       "2001:910:800::12"
		       "2001:910:800::40"))))
        (modify-services desktop-services
	  (guix-service-type config =>
	    (guix-configuration
	      (inherit config)
	      (authorized-keys
		(cons*
		  (local-file "../keys/ene.pub")
		  (local-file "../keys/xana.pub")
		  (local-file "../keys/cs-pc.pub")
		  %default-authorized-guix-keys))))
          (pulseaudio-service-type config =>
	    (pulseaudio-configuration
	      (inherit config)
              (script-file
                (local-file "../files/pulse-default.pa")))))))))
