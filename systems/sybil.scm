;;; Tyreunom's system administration and configuration tools.
;;;
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (gnu))
(use-modules (gnu system))
(use-modules (gnu bootloader) (gnu bootloader grub))
(use-modules (gnu services networking))
(use-modules (gnu services virtualization))
(use-modules (config os))

(let ((system (tyreunom-desktop-os "sybil")))
  (operating-system
    (inherit system)
    (keyboard-layout (keyboard-layout "fr" "bepo"))
    (bootloader
      (bootloader-configuration
        (target "/boot/efi")
        (bootloader grub-efi-bootloader)
	(keyboard-layout keyboard-layout)))
    (mapped-devices
      (list (mapped-device
              (source
                (uuid "7f0ef292-5d1f-4cde-8a59-074178c6903d"))
              (target "cryptroot")
              (type luks-device-mapping))))
    (file-systems
      (cons* (file-system
               (mount-point "/boot/efi")
               (device (uuid "C62C-8291" 'fat32))
               (type "vfat"))
             (file-system
               (mount-point "/")
               (device "/dev/mapper/cryptroot")
               (type "ext4")
               (dependencies mapped-devices))
             %base-file-systems))
    (users (map
	     (lambda (user)
	       (if (equal? (user-account-name user) "tyreunom")
		 (user-account
		   (inherit user)
		   (supplementary-groups
		     (cons* "libvirt" "kvm"
			    (user-account-supplementary-groups user))))
		 user))
	     (operating-system-users system)))
    (services
      (cons*
	(service libvirt-service-type
		 (libvirt-configuration
		   (unix-sock-group "libvirt")))
	(service virtlog-service-type
		 (virtlog-configuration
		   (max-clients 1000)))
	desktop-services))))
