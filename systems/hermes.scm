;;; Tyreunom's system administration and configuration tools.
;;;
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (gnu))
(use-modules (gnu system))
(use-modules (gnu bootloader) (gnu bootloader grub))
(use-modules (gnu services dns))
(use-modules (gnu services networking))
(use-modules (gnu services web))
(use-modules (config certbot) (config dns) (config mail) (config network) (config os)
             (config iptables) (config web))


;; NOTE: this config contains out-of band files.
;; To (re-)generate /etc/mail/dkim/private.key, run:
;; openssl genrsa -out /etc/mail/dkim/private.key 2048
;; openssl rsa -in /etc/mail/dkim/private.key -pubout -out /etc/mail/dkim/public.key
;; chmod 440 /etc/mail/dkim/private.key
;;
;; To (re-)generate /etc/knot/secrets.conf, run:
;; keymgt -t lepiller-key > /etc/knot/secrets.conf

(define lepiller-policy
  (append
    web-base-policy
    '("add_header Onion-Location 'http://c25o7knygjm3m67jy27yuynvv4pkfi25naucscmh4ubq2ggiig3v57ad.onion$request_uri';"
      "# accept-language: en,en-US;q=0.8,ja;q=0.6"
      "set $first_language '';"
      "if ($http_accept_language ~* '(en|eo|fr)') {"
      "  set $first_language $1;"
      "}"
      "if ($first_language = '') {"
      "  set $first_language 'en';"
      "}"
      "set $my_uri $first_language/$uri;"
      "location ~ ^/[^/]*$ {"
      "  return 301 /$first_language/$uri;"
      "}"
      )
    (web-html-policy
      '("    add_header Onion-Location 'http://c25o7knygjm3m67jy27yuynvv4pkfi25naucscmh4ubq2ggiig3v57ad.onion$request_uri';"
        "    try_files $my_uri $uri $uri/ =404;"))
    '("error_page 404 /404.html;")))

(operating-system
  (inherit (tyreunom-os "hermes"))
  (bootloader
    (bootloader-configuration
      (targets '("/dev/sda"))
      (bootloader grub-bootloader)))
  (file-systems (cons (file-system
                        (mount-point "/")
                        (device (uuid "27798665-5606-4fde-8da8-cc371e603892"))
                        (type "ext4"))
                      %base-file-systems))
  (services
    (append
      (list
        hermes-network-service
        lepiller-iptables-service
        (service tor-service-type)
        (tor-hidden-service "lepiller"
                            '((22 "127.0.0.1:22")
                              (80 "127.0.0.1:80")))
        (tor-hidden-service "mail"
                            '((25 "127.0.0.1:25")
                              (143 "127.0.0.1:143")
                              (587 "127.0.0.1:587")
                              (993 "127.0.0.1:993")))
        (service nginx-service-type
          (nginx-configuration
            (server-names-hash-bucket-size 128)))
        (service knot-service-type
                 (knot-configuration
                   (includes '("/etc/knot/secrets.conf"))
                   (acls (list master-acl))
                   (remotes (list ene))
                   (zones (list lepiller-master-zone
                                ipv4-reverse-master-zone
                                ipv6-reverse-master-zone))))
        (certbot-service `(("lepiller.eu" "www.lepiller.eu" "smtp.lepiller.eu")))
        (simple-service 'social-http-server nginx-service-type
          (list (nginx-server-configuration
                  (ssl-certificate "/etc/letsencrypt/live/social.lepiller.eu/fullchain.pem")
                  (ssl-certificate-key "/etc/letsencrypt/live/social.lepiller.eu/privkey.pem")
                  (listen '("443 ssl http2" "[::]:443 ssl http2"))
                  (server-name '("social.lepiller.eu"))
                  (root "/srv/http/social/public")
                  (locations
                    (list
		      (nginx-location-configuration
			(uri "/content/")
			(body '("alias /var/lib/social/user-data/public/;")))
                      (nginx-location-configuration
                        (uri "/")
                        (body '("proxy_pass http://localhost:8080;")))))
                  (raw-content default-web-policy))))
        (simple-service 'tor-lepiller-http-server nginx-service-type
          (list (nginx-server-configuration
                  (listen '("80" "[::]:80"))
                  (server-name '("c25o7knygjm3m67jy27yuynvv4pkfi25naucscmh4ubq2ggiig3v57ad.onion"))
                  (root "/srv/http/lepiller/public")
                  (index '("index.html"))
                  (try-files '("$uri" "$uri/" "=404"))
                  (raw-content lepiller-policy))))
        (simple-service 'lepiller-http-server nginx-service-type
          (list (nginx-server-configuration
                  (ssl-certificate "/etc/letsencrypt/live/lepiller.eu/fullchain.pem")
                  (ssl-certificate-key "/etc/letsencrypt/live/lepiller.eu/privkey.pem")
                  (listen '("443 ssl http2" "[::]:443 ssl http2"))
                  (server-name '("lepiller.eu" "www.lepiller.eu"))
                  (root "/srv/http/lepiller/public")
                  (index '("index.html"))
                  (try-files '("$uri" "$uri/" "=404"))
                  (raw-content lepiller-policy))))
        (simple-service 'default-http-server nginx-service-type
          (list (nginx-server-configuration
                  (ssl-certificate "/etc/letsencrypt/live/lepiller.eu/fullchain.pem")
                  (ssl-certificate-key "/etc/letsencrypt/live/lepiller.eu/privkey.pem")
                  (listen '("443 ssl http2" "[::]:443 ssl http2"))
                  (server-name '(default))
                  (root "/srv/http/default")
                  (raw-content default-web-policy)))))
      (lepiller-mail-services
        #:interface "ens18"
        #:domain "lepiller.eu")
      (server-services "hermes"))))
