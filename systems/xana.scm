;;; Tyreunom's system administration and configuration tools.
;;;
;;; Copyright © 2020 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (gnu))
(use-modules (gnu system))
(use-modules (gnu bootloader) (gnu bootloader grub))
(use-modules (gnu services mcron))
(use-modules (gnu services networking))
(use-modules (gnu services web))
(use-modules (config certbot) (config cuirass) (config network) (config os) (config web))
(use-modules (guix gexp))
(use-package-modules base compression gettext guile guile-xyz linux version-control
                     web wget)

(define lepiller-policy
  (append
    web-base-policy
    (web-html-policy
      '("    try_files $uri /$lang/$uri /$lang/$uri/index.html =404;"))
    '("error_page 404 /$lang/404.html;")))

(define (makefile-job directory target packages env)
  #~(lambda ()
      (define path
        (string-join
          (map (lambda (p) (string-append p "/bin")) (list #$@packages))
          ":"))
      (define (get-path dir)
        (string-join
          (map (lambda (p) (string-append p "/" path) (list #$@packages)))
          ":"))
      (setenv "PATH" path)
      (setenv "GIT_SSL_CAINFO" "/etc/ssl/certs/ca-certificates.crt")
      (setenv "SSL_CERT_DIR" "/etc/ssl/certs")
      (setenv "SSL_CERT_FILE" "/etc/ssl/certs/ca-certificates.crt")
      (setenv "LANG" "en_US.UTF-8")
      (for-each
        (lambda (env)
         (setenv (car env) (get-path (cdr env))))
        (quote #$env))
      (execl #$(file-append gnu-make "/bin/make") "make" "-C" #$directory #$target)))

(define i18n-download-job
  #~(job '(next-minute-from (next-hour '(05)) '(51))
         #$(makefile-job "/srv/http/i18n" "fdroid-update"
                         (list gnu-make coreutils findutils git)
                         '())))

(define i18n-compile-job
  #~(job '(next-minute-from (next-hour '(06)) '(21))
         #$(makefile-job "/srv/http/i18n" "all"
                         (list gnu-make coreutils findutils git grep util-linux)
                         '())))

(define nani-download-job
  #~(job '(next-minute-from (next-hour-from (next-day (range 1 31 5)) '(02)) '(12))
         #$(makefile-job "/srv/http/nani" "download"
                         (list gnu-make coreutils findutils grep gzip libiconv
                               sed tar unzip util-linux wget xz)
                         '())))

(define nani-update-job
  #~(job '(next-minute-from (next-hour-from (next-day (range 1 31 5)) '(03)) '(12))
         #$(makefile-job "/srv/http/nani" "all"
                         (list gnu-make coreutils findutils gnu-gettext grep guile-3.0
                               haunt sed)
                         '(("GUILE_LOAD_PATH" . "share/guile/site/3.0")
                           ("GUILE_LOAD_COMPILED_PATH" . "share/guile/site/3.0")))))

(operating-system
  (inherit (tyreunom-os "xana"))
  (bootloader
    (bootloader-configuration
      (targets '("/dev/sda"))
      (bootloader grub-bootloader)))
  (file-systems (cons (file-system
                        (mount-point "/")
                        (device (uuid "27207be8-f30a-4ac6-a5ec-41859e90ee94"))
                        (type "ext4"))
                      %base-file-systems))
  (swap-devices '("/dev/sda1"))
  (services
    (append
      (list
        (service dhcp-client-service-type)
        (service nginx-service-type
                 (nginx-configuration
                   (modules
                     (list
                       (file-append nginx-accept-language-module
                                    "/etc/nginx/modules/ngx_http_accept_language_module.so")))
                   (extra-content
                     (accept-languages
                       '(("en")
                         ("fr")
                         ("uk")
                         ("zh_CN" "zh-CN" "zh" "zh-Hans" "zh-Hans-CN"))))))
        (simple-service 'website-cron mcron-service-type
          (list i18n-download-job i18n-compile-job
                nani-download-job nani-update-job))
        (certbot-service `(("xana.lepiller.eu")
                           ("nani.lepiller.eu")
                           ("i18n.lepiller.eu")
                           ("offlate.lepiller.eu")))
        (simple-service 'nani-http-server nginx-service-type
          (list (nginx-server-configuration
                  (ssl-certificate
                    "/etc/letsencrypt/live/nani.lepiller.eu/fullchain.pem")
                  (ssl-certificate-key
                    "/etc/letsencrypt/live/nani.lepiller.eu/privkey.pem")
                  (listen '("443 ssl http2" "[::]:443 ssl http2"))
                  (server-name '("nani.lepiller.eu"))
                  (root "/srv/http/nani/public"))))
        (simple-service 'nani-http-server nginx-service-type
          (list (nginx-server-configuration
                  (ssl-certificate
                    "/etc/letsencrypt/live/offlate.lepiller.eu/fullchain.pem")
                  (ssl-certificate-key
                    "/etc/letsencrypt/live/offlate.lepiller.eu/privkey.pem")
                  (listen '("443 ssl http2" "[::]:443 ssl http2"))
                  (server-name '("offlate.lepiller.eu"))
                  (root "/srv/http/offlate/public")
                  (index '("index.html"))
                  (try-files '("$uri" "$uri/" "=404"))
                  (raw-content lepiller-policy))))
        (simple-service 'i18n-http-server nginx-service-type
          (list (nginx-server-configuration
                  (ssl-certificate
                    "/etc/letsencrypt/live/i18n.lepiller.eu/fullchain.pem")
                  (ssl-certificate-key
                    "/etc/letsencrypt/live/i18n.lepiller.eu/privkey.pem")
                  (listen '("443 ssl http2" "[::]:443 ssl http2"))
                  (server-name '("i18n.lepiller.eu"))
                  (root "/srv/http/i18n/public"))))
        (simple-service 'default-http-server nginx-service-type
          (list (nginx-server-configuration
                  (ssl-certificate
                    "/etc/letsencrypt/live/xana.lepiller.eu/fullchain.pem")
                  (ssl-certificate-key
                    "/etc/letsencrypt/live/xana.lepiller.eu/privkey.pem")
                  (listen '("443 ssl http2" "[::]:443 ssl http2"))
                  (server-name '(default "xana.lepiller.eu"))
                  (root "/srv/http/default")))))
      (modify-services (server-services "xana")
	(guix-service-type config =>
	  (guix-configuration
	    (inherit config)
	    (authorized-keys
	      (cons
		(local-file "../keys/tachikoma.pub")
		%default-authorized-guix-keys))))))))
