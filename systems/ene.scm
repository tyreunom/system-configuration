;;; Tyreunom's system administration and configuration tools.
;;;
;;; Copyright © 2019, 2020 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (gnu))
(use-modules (gnu bootloader))
(use-modules (gnu bootloader u-boot))
(use-modules (gnu packages bootloaders))
(use-modules (gnu packages dns))
(use-modules (gnu services dns))
(use-modules (gnu services mail))
(use-modules (gnu services networking))
(use-modules (gnu services version-control))
(use-modules (gnu services web))
(use-modules (gnu system))

(use-modules (gnu packages libunwind))
(use-modules (gnu packages mail))
(use-modules (gnu packages tor))
(use-modules (gnu packages web))
(use-modules (guix packages))
(use-modules (guix utils))
(use-modules (guix transformations))

(use-modules (config certbot) (config dns) (config iptables)
             (config mail) (config os) (config static-web)
	     (config web)
	     (services gitile) (packages gitile))

;; Copy from (gnu bootloader u-boot)
(define install-allwinner-u-boot
  #~(lambda (bootloader device mount-point)
      (let ((u-boot (string-append bootloader
                                   "/libexec/u-boot-sunxi-with-spl.bin")))
        (write-file-on-device u-boot (stat:size (stat u-boot))
                              device (* 8 1024)))))

(define u-boot-cubietruck-bootloader
  (bootloader
    (inherit u-boot-bootloader)
    (package u-boot-cubietruck)
    (installer install-allwinner-u-boot)))

(operating-system
  (inherit (tyreunom-os "ene"))
  (bootloader
    (bootloader-configuration
      (target "/dev/mmcblk0")
      (bootloader u-boot-cubietruck-bootloader)))
  (initrd-modules (cons* "sunxi-mmc" "sd_mod" "ahci_sunxi" %base-initrd-modules))
  (file-systems (cons (file-system
                        (mount-point "/")
                        (device "/dev/sda1")
                        (type "ext4"))
                      %base-file-systems))
  (services
    (append
      (list
        (service dhcp-client-service-type)
        lepiller-iptables-service
        (agetty-service
          (agetty-configuration
            (extra-options '("-L"))
            (baud-rate "115200")
            (term "vt100")
            (tty "ttyS0")))
        (service tor-service-type
		 (tor-configuration
		   (tor (package
			  (inherit tor)
			  (arguments
			   `(#:tests? #f
			     ,@(package-arguments tor)))))))
        (tor-hidden-service "mail"
                            '((25 "127.0.0.1:25")
                              (143 "127.0.0.1:143")
                              (587 "127.0.0.1:587")
                              (993 "127.0.0.1:993")))
        (service knot-service-type
                 (knot-configuration
		   (knot
		     ((options->transformation '((without-tests . "protobuf")))
		       knot))
                   (includes '("/etc/knot/secrets.conf"))
                   (acls (list master-acl))
                   (remotes (list hermes))
                   (zones (list lepiller-master-zone
                                ipv4-reverse-master-zone
                                ipv6-reverse-master-zone))))
        (certbot-service `(("courriel.lepiller.eu" "imap.lepiller.eu")
                           ("ene.lepiller.eu" "rennes.lepiller.eu")
                           ("avatar.lepiller.eu")
                           ("git.lepiller.eu")
			   ("social.lepiller.eu")))
        (service nginx-service-type)
        (service php-fpm-service-type
		 (php-fpm-configuration
		   ;; TODO: remove after guix doesn't complain about duplicate
		   ;; groups.
		   (group "php-fpm2")))
        (service fcgiwrap-service-type
                 (fcgiwrap-configuration
                   (group "git")))
        (cat-avatar-generator-service
          #:configuration
          (nginx-server-configuration
            (server-name '("avatar.lepiller.eu"))
            (ssl-certificate
              "/etc/letsencrypt/live/avatar.lepiller.eu/fullchain.pem")
            (ssl-certificate-key
              "/etc/letsencrypt/live/avatar.lepiller.eu/privkey.pem")
            (listen '("443 ssl http2" "[::]:443 ssl http2"))))
        (simple-service 'default-http-server nginx-service-type
          (list (nginx-server-configuration
                  (ssl-certificate
                    "/etc/letsencrypt/live/ene.lepiller.eu/fullchain.pem")
                  (ssl-certificate-key
                    "/etc/letsencrypt/live/ene.lepiller.eu/privkey.pem")
                  (listen '("443 ssl http2" "[::]:443 ssl http2"))
                  (server-name '(default))(root "/srv/http/default"))))
        (simple-service 'social-http-server nginx-service-type
          (list (nginx-server-configuration
                  (ssl-certificate "/etc/letsencrypt/live/social.lepiller.eu/fullchain.pem")
                  (ssl-certificate-key "/etc/letsencrypt/live/social.lepiller.eu/privkey.pem")
                  (listen '("443 ssl http2" "[::]:443 ssl http2"))
                  (server-name '("social.lepiller.eu"))
                  (root "/srv/http/social/public")
                  (locations
                    (list
		      (nginx-location-configuration
			(uri "/content/")
			(body '("alias /var/lib/social/user-data/public/;")))
                      (nginx-location-configuration
                        (uri "/")
                        (body '("proxy_pass http://localhost:8081;")))))
                  (raw-content default-web-policy))))
	(service gitile-service-type)
        (simple-service 'git-server nginx-service-type
          (list (nginx-server-configuration
                  (ssl-certificate
                    "/etc/letsencrypt/live/git.lepiller.eu/fullchain.pem")
                  (ssl-certificate-key
                    "/etc/letsencrypt/live/git.lepiller.eu/privkey.pem")
                  (listen '("443 ssl http2" "[::]:443 ssl http2"))
                  (server-name '("git.lepiller.eu"))
                  (root "/srv/http/git/public")
                  (locations
                    (append
                      (list
                        (git-http-nginx-location-configuration
                          (git-http-configuration
                            (uri-path "/git/")
                            (git-root "/var/lib/gitolite/repositories")))
                        (nginx-location-configuration
                          (uri "/")
                          (body
                            (list
                              "proxy_pass http://127.0.0.1:8080/;"))))
                      (map
                        (lambda (loc)
                          (nginx-location-configuration
                            (uri loc)
                            (body
                              (list
                                "root /srv/http/git/public;"))))
                        '("/css" "/js" "/images" "~* .*/manual/.*" "= /"
                          "= /index.html")))))))
        (service static-web-site-service-type
                 (static-web-site-configuration
                  (git-url "https://git.lepiller.eu/git/guile-netlink")
                  (git-ref '(branch . "master"))
                  (directory "/srv/http/git/guile-netlink-manual")
                  (build-file "doc/build.scm")))
        ;; on activation, gitolite chmods its home directory to #o700, disabling
        ;; access to git-http-backend.  Re-enable that access.
        (simple-service 'gitolite-home-permissions
                        activation-service-type
                        #~(chmod "/var/lib/gitolite" #o750))
        (service gitolite-service-type
          (gitolite-configuration
            (admin-pubkey (local-file "../keys/tyreunom.pub"))
            (rc-file
              (gitolite-rc-file
                (umask #o0027)
                (git-config-keys ".*"))))))
      (modify-services
        (lepiller-mail-services
          #:interface "eth0"
          #:domain "courriel.lepiller.eu")
        (dovecot-service-type config =>
         (dovecot-configuration
           (inherit config)
           (dovecot (package
                      (inherit dovecot)
                      (arguments
                        `(#:tests? #f
                          ,@(package-arguments dovecot))))))))
      (server-services "ene"))))
