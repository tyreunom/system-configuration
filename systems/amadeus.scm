;;; Tyreunom's system administration and configuration tools.
;;;
;;; Copyright © 2020 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (gnu))
(use-modules (gnu system))
(use-modules (gnu bootloader) (gnu bootloader grub))
(use-modules (gnu services networking))
(use-modules (gnu services web))
(use-modules (config certbot) (config cuirass) (config network) (config os))

(operating-system
  (inherit (tyreunom-os "amadeus"))
  (bootloader
    (bootloader-configuration
      (target "/dev/sda")
      (bootloader grub-bootloader)))
  (file-systems (cons (file-system
                        (mount-point "/")
                        (device (uuid "03dd5046-be04-4853-ae65-47f79584133f"))
                        (type "ext4"))
                      %base-file-systems))
  (swap-devices '("/dev/sda1"))
  (services
    (append
      (list
        (service dhcp-client-service-type)
        (service nginx-service-type
	  (nginx-configuration
	    (extra-content
	      (string-join %cuirass-extra-content "\n"))))
        (certbot-service `(("amadeus.lepiller.eu")
                           ("guix.lepiller.eu")))
        (simple-service 'default-http-server nginx-service-type
          (list (nginx-server-configuration
                  (ssl-certificate
                    "/etc/letsencrypt/live/amadeus.lepiller.eu/fullchain.pem")
                  (ssl-certificate-key
                    "/etc/letsencrypt/live/amadeus.lepiller.eu/privkey.pem")
                  (listen '("443 ssl http2" "[::]:443 ssl http2"))
                  (server-name '(default "amadeus.lepiller.eu"))
                  (root "/srv/http/default")))))
      (cuirass-services
        "guix.lepiller.eu"
        "/etc/letsencrypt/live/guix.lepiller.eu/fullchain.pem"
        "/etc/letsencrypt/live/guix.lepiller.eu/privkey.pem")
      (modify-services (server-services "amadeus")
        (guix-service-type config =>
          (guix-configuration
            (inherit config)
            (substitute-urls '())
	    (extra-options '("-M2" "--no-substitutes"))))))))
