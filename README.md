Tyreunom's System Configuration
===============================

This repository contains the tools and configurations of my
[Guix](https://gnu.org/s/guix) systems. Guix provides a complete solution for
declarative system configuration while allowing the full power of a
programming language. A system configuration is nothing less than a complete
guile program!

In addition to the declarative nature of Guix configurations, Guix itself
provides a robust system, with reproducibility, statelessness and an emphasis on
user freedom, thanks to its functional package management technique.

Understanding the Content of this Repository
--------------------------------------------

Although the repo might be used for inspiration, it's not meant to be
generic in any way: it's only my configuration.

The `modules` directory contains guile modules that are used by multiple
systems. They are loaded in the guile PATH along with guix modules during
the call to the `guix system` command that generates the system.

The `motd` and `keys` directories contain data files that are meant to be
copied directly to the store, thanks to `local-file`.

The `systems` directory contain the actual configurations, one file per
system, named after the host-name of the system.

How to Configure a System
-------------------------

Assuming you cloned this repository in `/root/system-configurations`, you can
reconfigure your system with this command:

```bash
guix system reconfigure -L /root/system-configurations/modules \
  /root/system-configurations/systems/<system>.scm
```

User Configuration
------------------

Guix doesn't provide a nice way to configure user services (yet), so
the `homes` directory contains configuration and configuraton files, as
well as manifests for users.

This is mostly useful for my desktop computers.

I found a way to manage the home directory with guix, but it's not as
nice as it could be. The home directory is transformed into a guix profile,
and to do that, a package is created that contains all my user configuration.

First, create a separate directory that will contain all your data that
can be written to: `mkdir -p /data/tyreunom` in my case. Then, create
a new profile, that will only contain one package:

```
cd homes
guix package -p /var/guix/profiles/per-user/tyreunom/home -L ../modules \
  -f sybil-home.scm
```

My home directory is now read-only which a lot of software doesn't like.
The package contains some tweaks to have some writable directories, but
not everything will work. It's still a work in progress!

As an example of something that will not work properly, pulseaudio will
simply refuse to run if your home directory is read-only. To work around
this, I start pulseaudio from my window manager's autostart file, with
a separate HOME variable set to a directory in `/tmp`.

A nice aspect of this however, is that we can now create more modules to
support more software. These modules can be generic enough to be
useful to anyone with a simple configuration, just like services are
useful for system administrators. Home related modules can be found
in `(home)`, that is in `modules/home`.

What do your System Names Reference?
------------------------------------

My systems are named after AI systems / personalities in anime and books
I've seen and read.

* Hermes (エルメス) is Kino's motorrad in
  [Kino's Journey](https://en.wikipedia.org/wiki/Kino%27s_Journey).
  It's not explicitely said that it's an AI, but come on, a talking bike?
* Xana (X.A.N.A.) is the rogue AI in
  [Code Lyoko](https://en.wikipedia.org/wiki/Code_lyoko). OK, this is not
  exactly an anime, but I enjoyed watching it as a kid :).
* Ene (エネ) is a former human who gained the ability to live in digital
  devices, in [Mekakucity Actors](https://en.wikipedia.org/wiki/Kagerou_Project).
  Since she lives in the main protagonist's computer in the anime, I think
  it's a fitting name for my home server.
* Lobsang is the name of an AI in
  [the Long Earth](https://en.wikipedia.org/wiki/The_Long_Earth). He claims to
  be the reincarnation of a Tibetan motorcycle repairman. You won't find a
  system configuration because it's a laptop that's running
  [Haiku](https://haiku-os.org), not a Guix system.
* Tachikoma is the name of a kind of a military tank equipped with AI in
  [Ghost in the Shell](https://en.wikipedia.org/wiki/Ghost_in_the_Shell).
* Amadeus is the name of an AI system in
  [Steins Gate 0](https://en.wikipedia.org/wiki/Steins;Gate_0_(TV_series)).
