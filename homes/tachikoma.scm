(use-modules
  (gnu home)
  (gnu services)
  (gnu packages)
  (gnu home services)
  (gnu home services openbox)
  (gnu home services shells)

  ;; for openbox
  (gnu packages compton)
  (gnu packages dunst)
  (gnu packages game-development)
  (gnu packages games)
  (gnu packages geo)
  (gnu packages gnucash)
  (gnu packages gnuzilla)
  (gnu packages ibus)
  (gnu packages image-viewers)
  (gnu packages inkscape)
  (gnu packages lxde)
  (gnu packages mail)
  (gnu packages messaging)
  (gnu packages minetest)
  (gnu packages password-utils)
  (gnu packages pulseaudio)
  (gnu packages sync)
  (gnu packages xfce)
  (games packages 7-billion-humans)
  (games packages baba-is-you)
  (games packages factorio)
  (games packages hollow-knight)
  (games packages mini-metro)
  (games packages starsector)
  (games packages sunless-skies)
  (nongnu packages steam-client)
  (guix gexp)
  )

(define openbox-environment
  (plain-file "environment" "eval $(ssh-agent)
export GTK_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
export QT_IM_MODULE=ibus
export GUIX_GTK2_IM_MODULE_FILE=/home/tyreunom/.guix-profile/lib/gtk-2.0/2.10.0/immodules-gtk2.cache
export GUIX_GTK3_IM_MODULE_FILE=/home/tyreunom/.guix-profile/lib/gtk-3.0/3.0.0/immodules-gtk3.cache"))

(define openbox-autostart
  (computed-file "autostart"
     #~(with-output-to-file #$output
         (lambda _
           (format #t "HOME=/tmp/pa ~a --start~%" #$(file-append pulseaudio "/bin/pulseaudio"))
           (format #t "~a --bg-fill ~a~%" #$(file-append feh "/bin/feh")
                   #$(local-file "/data/tyreunom/background.png"))
           (format #t "~a -CGb~%" #$(file-append compton "/bin/compton"))
           (format #t "~a -conf ~a &~%" #$(file-append dunst "/bin/dunst")
                   #$(plain-file "dunstrc" ""))
           (format #t "export GTK_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
export QT_IM_MODULE=ibus
export GUIX_GTK2_IM_MODULE_FILE=~~/.guix-profile/lib/gtk-2.0/2.10.0/immodules-gtk2.cache
export GUIX_GTK3_IM_MODULE_FILE=~~/.guix-profile/lib/gtk-3.0/3.0.0/immodules-gtk3.cache
IBUS_COMPONENT_PATH=~~/.guix-profile/share/ibus/component ~a -drx~%" #$(file-append ibus "/bin/ibus-daemon"))))))
(define openbox-rc (local-file "files/openbox/rc.xml"))

(define tyreunom-openbox-configuration
  (home-openbox-configuration
    (autostart openbox-autostart)
    (environ openbox-environment)
    (rc openbox-rc)
    (menus
      (list
        (home-openbox-menu
          (id "apps-game-menu")
          (label "Jeux")
          (elements
            (list
              (home-openbox-element-execute
                (label "MineTest")
                (command (file-append minetest "/bin/minetest")))
              ;(home-openbox-element-execute
              ;  (label "Starsector")
              ;  (command (file-append starsector "/bin/starsector")))
              ;(home-openbox-element-execute
              ;  (label "Factorio")
              ;  (command (file-append factorio "/bin/factorio")))
              ;(home-openbox-element-execute
              ;  (label "Hollow Knight")
              ;  (command (file-append gog-hollow-knight "/bin/hollow-knight")))
              ;(home-openbox-element-execute
              ;  (label "7 Billion Humans")
              ;  (command (file-append gog-7-billion-humans "/bin/7-billion-humans")))
              ;(home-openbox-element-execute
              ;  (label "Sunless Skies")
              ;  (command (file-append gog-sunless-skies "/bin/sunless-skies")))
              ;(home-openbox-element-execute
              ;  (label "Baba is You")
              ;  (command (file-append baba-is-you "/bin/baba-is-you")))
              ;(home-openbox-element-execute
              ;  (label "Mini Metro")
              ;  (command (file-append mini-metro "/bin/mini-metro")))
              ;(home-openbox-element-execute
              ;  (label "0ad")
              ;  (command (file-append 0ad "/bin/0ad")))
              ;(home-openbox-element-execute
              ;  (label "godot")
              ;  (command (file-append godot "/bin/godot"))))))
              )))
        (home-openbox-menu
          (id "apps-other-menu")
          (label "Autres")
          (elements
            (list
              (home-openbox-element-execute
                (label "Pavucontrol")
                (command (file-append pavucontrol "/bin/pavucontrol")))
              (home-openbox-element-execute
                (label "Owncloud")
                (command (file-append owncloud-client "/bin/owncloud")))
              (home-openbox-element-execute
                (label "Keepass")
                (command (file-append keepassxc "/bin/keepassxc")))
              (home-openbox-element-execute
                (label "Claws-mail")
                (command (file-append claws-mail "/bin/claws-mail")))
              (home-openbox-element-execute
                (label "Hexchat")
                (command (file-append hexchat "/bin/hexchat")))
              (home-openbox-element-execute
                (label "GnuCash")
                (command (file-append gnucash "/bin/gnucash")))
              (home-openbox-element-execute
                (label "JOSM")
                (command (file-append josm "/bin/josm")))
              (home-openbox-element-execute
                (label "Inkscape")
                (command (file-append inkscape "/bin/inkscape"))))))))
    (root-elements
      (list
        (home-openbox-element-menu (id "apps-game-menu"))
        (home-openbox-element-menu (id "apps-other-menu"))
        (home-openbox-element-execute
          (label "Terminal")
          (command (file-append xfce4-terminal "/bin/xfce4-terminal")))
        (home-openbox-element-execute
          (label "Navigateur")
          (command (file-append icecat "/bin/icecat -P --no-remote")))
        (home-openbox-element-execute
          (label "Gestionnaire de fichiers")
          (command (file-append pcmanfm "/bin/pcmanfm")))))))


(home-environment
  (packages
    (map specification->package+output
         (list "arc-theme"
               "arc-icon-theme"
               "hicolor-icon-theme"

               ;; utilities
               "ibus"
               "ibus-anthy"

               ;; gui
               "feh"
               "mpv"
               "syncthing-gtk"
               "syncthing"
               "virt-manager"
               "zathura-pdf-poppler"
               "zathura"

               ;; cli
               "graphviz"
               "imagemagick"
               "pinentry"
               "youtube-dl"
               "adb"
               "git:send-email"
               "git"
               "bind:utils"
               "gnupg"
               "curl"
               "python"
               "wget"
               "unzip"
               "torsocks"
               "strace"
               "acpi"
               "setxkbmap"
               "file"

               ;; guile
               "guile"
               "guile-readline"

               ;; editor
               "python-pynvim"
               "neovim"

               ;; fonts
               "font-terminus"
               "font-cozette"
               "font-linuxlibertine"
               "font-tamzen"
               "font-liberation"
               "font-fira-code"
               "font-inconsolata"
               "font-mplus-testflight"
               "font-fontna-yasashisa-antique"
               "font-dejavu"
               "font-ipa-mj-mincho"
               "font-wqy-microhei"
               "font-google-noto"

               ;; games
               "starsector"
               ;"baba-is-you"
               "steam"

               "glibc-locales"
               "hunspell-dict-fr-moderne"
               "hunspell-dict-fr-reforme1990"
               "hunspell-dict-fr")))
  (services
    (list (service home-bash-service-type
                   (home-bash-configuration
                     (environment-variables
                       `(("GTK_IM_MODULE" . "ibus")
                         ("XMODIFIERS" . "@im=ibus")
                         ("QT_IM_MODULE" . "ibus")
                         ("GUIX_GTK2_IM_MODULE_FILE" . "/home/tyreunom/.guix-profile/lib/gtk-2.0/2.10.0/immodules-gtk2.cache")
                         ("GUIX_GTK3_IM_MODULE_FILE" . "/home/tyreunom/.guix-profile/lib/gtk-3.0/3.0.0/immodules-gtk3.cache")
                         ("EDITOR" . "nvim")))
                     (bashrc
                      (list
                        (plain-file "aliases"
                                    "alias vim=nvim
alias info=\"info --vi-keys\"")))))
          (service home-openbox-service-type tyreunom-openbox-configuration)
           (simple-service 'nvim-config home-files-service-type
             `(("config/nvim/init.vim" ,(plain-file "init.vim" "set tabstop=4
set colorcolumn=80
set expandtab
autocmd FileType latex,tex,text,md,markdown setlocal spell"))))
          (simple-service 'xfce4-terminal home-files-service-type
            `(("config/xfce4/terminal/terminalrc" ,(local-file "files/xfce4-terminal/terminalrc"))))
          (simple-service 'guile home-files-service-type
            `(("guile" ,(local-file "files/guilerc"))))
          (simple-service 'git home-files-service-type
            `(("gitconfig" ,(local-file "files/gitconfig"))))
          (simple-service 'gtk home-files-service-type
            `(("gtkrc-2.0" ,(plain-file "gtkrc-2.0"
                                        "gtk-theme-name=\"Arc-Dark\"
gtk-icon-theme-name=\"Arc\""))
              ("config/gtk-3.0/settings.ini" ,(plain-file "settings.ini"
                                                          "[Settings]
gtk-theme-name=Arc-Dark
gtk-icon-theme-name=Arc")))))))
