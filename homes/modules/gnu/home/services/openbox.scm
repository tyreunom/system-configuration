(define-module (gnu home services openbox)
  #:use-module (guix build utils)
  #:use-module (guix gexp)
  #:use-module (guix licenses)
  #:use-module (guix packages)
  #:use-module (guix records)
  #:use-module (gnu home services)
  #:use-module (ice-9 match)
  #:export (home-openbox-configuration
            home-openbox-configuration?
            home-openbox-configuration-autostart
            home-openbox-configuration-environ
            home-openbox-configuration-menu
            home-openbox-configuration-rc
            
            home-openbox-menu
            home-openbox-menu-id
            home-openbox-menu-label
            home-opebnox-menu-elements
            
            home-openbox-element-menu
            home-openbox-element-execute
            
            home-openbox-service-type))

(define-record-type* <home-openbox-configuration>
  home-openbox-configuration make-home-openbox-configuration
  home-openbox-configuration?
  (autostart     home-openbox-configuration-autostart
                 (default #f))
  (environ       home-openbox-configuration-environ
                 (default #f))
  (menus         home-openbox-configuration-menus
                 (default '()))
  (root-elements home-openbox-configuration-root-elements
                 (default '()))
  (rc            home-openbox-configuration-rc
                 (default #f)))

(define-record-type* <home-openbox-menu>
  home-openbox-menu make-home-openbox-menu
  home-openbox-menu?
  (id       home-openbox-menu-id)
  (label    home-openbox-menu-label)
  (elements home-openbox-menu-elements))

(define-record-type* <home-openbox-element-menu>
  home-openbox-element-menu make-home-openbox-element-menu
  home-openbox-element-menu?
  (id home-openbox-element-menu-id))

(define-record-type* <home-openbox-element-execute>
  home-openbox-element-execute make-home-openbox-element-execute
  home-openbox-element-execute?
  (label   openbox-element-execute-label)
  (command openbox-element-execute-command)
  (notify? openbox-element-execute-notify?
           (default #t)))

(define (generate-openbox-menu menus root-menu)
  (define (element->item element)
    (match element
      (($ <home-openbox-element-menu> id)
       `(menu (@ (id ,id))))
      (($ <home-openbox-element-execute> label command notify?)
       `(item (@ (label ,label))
          (action (@ (name "Execute"))
            (command ,command)
            ,@(if notify? '(startupnotify (enabled "yes")) '()))))))
  #~(begin
      (use-modules (sxml simple))
      (with-output-to-file #$output
         (lambda _
           (sxml->xml
             `(openbox_menu (@ (xmlns "http://openbox.org/3.4/menu"))
                #$(map
                   (lambda (menu)
                     `(menu (@ (id ,(home-openbox-menu-id menu)) (label ,(home-openbox-menu-label menu)))
                        ,(map element->item (home-openbox-menu-elements menu))))
                   menus)
                (menu (@ (id "root-menu") (label "Openbox 3"))
                  #$(map element->item root-menu))))))))

(define (openbox-autostart autostart)
  (match autostart
    (#f (plain-file "autostart" ""))
    (_ autostart)))

(define (openbox-environment environ)
  (match environ
    (#f (plain-file "environ" ""))
    (_ environ)))

(define (openbox-rc rc)
  (match rc
    (#f (plain-file "rc.xml" ""))
    (_ rc)))

(define (add-openbox-configuration config)
  (match config
    (($ <home-openbox-configuration> autostart environ menus root-elements rc)
     `(("config/openbox/menu.xml"
        ,(computed-file "menu.xml" (generate-openbox-menu menus root-elements)))
       ("config/openbox/autostart" ,(openbox-autostart autostart))
       ("config/openbox/environment" ,(openbox-environment environ))
       ("config/openbox/rc.xml" ,(openbox-rc rc))))))

(define home-openbox-service-type
  (service-type (name 'home-openbox)
                (extensions
                 (list (service-extension
                        home-files-service-type
                        add-openbox-configuration)))
                (default-value (home-openbox-configuration))
                (description "Configure Openbox")))
